# Pytest Selenium Template

## Prerequisites

- Python 3.7+
- PyCharm
- Up-to-date `Chrome`, `Firefox` and `Edge` browsers
- Terminal emulator

## Windows

- Open terminal (e.g. [cmder](https://blog.qalabs.pl/narzedzia/git-cmder/))
- Prepare virtual environment:

	venv-install.bat

- Install dependencies:

	pip install -r requirements.txt
	
- Run all tests:

    pytest tests


```
❯ pytest
❯ pytest
=== test session starts ===
collected 3 items

tests/test_hello_selenium_chrome.py .    [ 33%]
tests/test_hello_selenium_edge.py .      [ 66%]
tests/test_hello_selenium_firefox.py .   [100%]

==== 3 passed in 10.22s ====
```

- Import project to `PyCharm`

## macOS

- Open terminal
- Prepare virtual environment

	python -m venv venv
	source venv/bin/activate
	

- Install dependencies:

	pip install -r requirements.txt

- Run all tests:

    pytest tests
    
- Import project to PyCharm

# See also

- https://blog.qalabs.pl/narzedzia/python-pycharm
- https://blog.qalabs.pl/pytest/pytest-pierwsze-kroki
- https://blog.qalabs.pl/python-selenium/webdriver-manager-pierwsze-kroki
