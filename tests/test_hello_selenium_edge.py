import pytest
from selenium.webdriver import Edge
from selenium.webdriver.common.by import By


@pytest.fixture()
def driver():
    d = Edge()
    d.implicitly_wait(1)
    d.get("https://qalabs.pl")
    yield d
    d.quit()


def test_hello_selenium_edge(driver):
    assert driver.title == "QA Labs - Warsztaty dla Specjalistów IT"
    assert driver.find_element(By.CSS_SELECTOR, "#page-top > header div.intro-heading").text == "QA Labs"
    assert driver.find_element(By.CSS_SELECTOR, "#page-top > header div.intro-lead-in").text == "Automatyzacja aplikacji internetowych w technologii Java i Python"
